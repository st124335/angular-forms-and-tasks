import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactiveforms',
  templateUrl: './reactiveforms.component.html',
  styleUrls: ['./reactiveforms.component.css'],
  standalone: false
})
export class ReactiveformsComponent {
  name = new FormControl('jj'); //initial value in those ''

  log_in = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  onSubmit(){
    // TODO: Use EventEmitter with form value
    console.warn(this.log_in.value);
    console.log('already log-in:', this.log_in);
  }

}
