import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-si',
  templateUrl: './demo-si.component.html',
  styleUrls: ['./demo-si.component.css']
})
export class DemoSIComponent {
  Name = "somesh";
  time: string = "1pm";

  FSAD ={
    name:'Somesh',
    classroom:'CS106',
    student:16,
    classimage: '/assets/classroom.png'
  }

  whenchanged(event:any){
    // this.Name = "Mr.Coka"

    // console.log(event)

    this.time = event.target.value

  }

}
