import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSIComponent } from './demo-si.component';

describe('DemoSIComponent', () => {
  let component: DemoSIComponent;
  let fixture: ComponentFixture<DemoSIComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DemoSIComponent]
    });
    fixture = TestBed.createComponent(DemoSIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
