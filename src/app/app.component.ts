import { Component } from '@angular/core';
import { NgForm, ValidatorFn, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  registrationModel = {
    username: '',
    email: '',
    password: '',
    confirmPassword: ''
  };

  passwordMatchValidator: ValidatorFn = (control: AbstractControl) => {
    const password = control.get('password')?.value;
    const confirmPassword = control.get('confirmPassword')?.value;

    return password === confirmPassword ? null : { 'passwordMismatch': true };
  };

  onSubmitRegistration(registrationForm: NgForm) {
    if (this.registrationModel.password !== this.registrationModel.confirmPassword) {
      registrationForm.controls['confirmPassword'].setErrors({ 'passwordMismatch': true });
    } else {
      console.log('Registration form submitted:', this.registrationModel);
    }
  }
}
