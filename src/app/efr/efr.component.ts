import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-efr',
  templateUrl: './efr.component.html',
  styleUrls: ['./efr.component.css']
})
export class EfrComponent {
  // onClick(){
  //   console.log('Button clicked!');
  // }

  // onKeyUp(event: any){
  //   console.log('Key up:', event.target.value);
  // }

  

//   title = "Template driven forms";

//   countryList:country[] = [
//     new country("1", "India"),
//     new country("2", 'USA'),
//     new country("3", 'England')
//   ];
// }

// export class country {
//   id:string;
//   name:string;

//   constructor(id:string, name:string){
//     this.id = id;
//     this.name = name;
//   }


  user = {
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
    isRemember: [false]
  };

  onSubmit() {
    // Handle form submission logic here
    console.log('Form submitted:', this.user);
  }

  password: string = ''; // Assuming you have a password input
  confirmPassword: string = '';
  
  


}


