import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopheaderComponent } from './topheader/topheader.component';
import { DemoSIComponent } from './demo-si/demo-si.component';
import { HighlightDirective } from './highlight.directive';
import { EfrComponent } from './efr/efr.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // Import the FormsModule
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { ReactiveformsComponent } from './reactiveforms/reactiveforms.component';





@NgModule({
  declarations: [
    AppComponent,
    TopheaderComponent,
    DemoSIComponent,
    HighlightDirective,
    EfrComponent,
    LoginFormComponent,
    RegistrationFormComponent,
    ReactiveformsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, // Add FormsModule to the imports array
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
